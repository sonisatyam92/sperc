package org.sperc.controller.contact;

import org.sperc.controller.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Satyam Soni on 4/11/2015.
 */
@Controller
public class ContactUsController extends AbstractController {

    @RequestMapping(value = "/ContactUs")
    public ModelAndView contactUs(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession){
        populateCommonModel(model);
        return new ModelAndView("contact-us");
    }
}
