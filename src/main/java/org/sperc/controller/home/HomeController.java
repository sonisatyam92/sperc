package org.sperc.controller.home;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sperc.bean.CourseBean;
import org.sperc.controller.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Satyam Soni on 4/11/2015.
 */
@Controller
public class HomeController extends AbstractController {
    private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);

    @RequestMapping(value = "/home")
    public ModelAndView home(Model model ,HttpServletRequest request, HttpServletResponse response, HttpSession httpSession){

        populateCommonModel(model);
        return new ModelAndView("home");
    }
}
