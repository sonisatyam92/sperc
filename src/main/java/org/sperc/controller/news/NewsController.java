package org.sperc.controller.news;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sperc.controller.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Satyam Soni on 4/26/2015.
 */
@Controller
public class NewsController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(NewsController.class);

    @RequestMapping(value = "/news")
    public ModelAndView faculty(Model model, HttpServletRequest request, HttpServletResponse response){

        populateCommonModel(model);
        return new ModelAndView("news");

    }
}


