package org.sperc.controller.event;

import org.sperc.controller.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Satyam Soni on 4/22/2015.
 */
@Controller
public class EventController extends AbstractController {


    @RequestMapping(value = "/events")
    public ModelAndView getCareer(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession){
        populateCommonModel(model);

        return new ModelAndView("events");
    }

    @RequestMapping(value = "/viewEvent")
    public ModelAndView getCareer(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession,
                                  @RequestParam("eventId") Integer eventId){
        populateCommonModel(model);

        return new ModelAndView("events");
    }
}
