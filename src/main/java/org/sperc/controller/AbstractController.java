package org.sperc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sperc.bean.CourseBean;
import org.sperc.bean.FacultyBean;
import org.sperc.common.AppConstants;
import org.sperc.service.ConfigService;
import org.sperc.service.CourseService;
import org.sperc.service.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import java.util.List;
import java.util.Map;
import static org.sperc.common.AppConstants.*;
/**
 * Created by Satyam Soni on 4/11/2015.
 */

public abstract class AbstractController implements BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractController.class);

    @Autowired
    private ConfigService configService;

    public ConfigService getConfigService() {
        return configService;
    }

    public void setConfigService(ConfigService configService) {
        this.configService = configService;
    }

    public Map<String,String> getConfig() throws Exception {

        if(CONFIG_MAP == null){
            synchronized (CONFIG_MAP){
                if(CONFIG_MAP == null){
                    CONFIG_MAP = getConfigService().getConfigMap();
                }
            }
        }
        return CONFIG_MAP;
    }

    @Autowired
    private CourseService courseService;

    public CourseService getCourseService() {
        return courseService;
    }

    public void setCourseService(CourseService courseService) {
        this.courseService = courseService;
    }

    @Autowired
    private FacultyService facultyService;

    public FacultyService getFacultyService() {
        return facultyService;
    }

    public void setFacultyService(FacultyService facultyService) {
        this.facultyService = facultyService;
    }


    protected void populateCommonModel(Model model){
        try {
            List<CourseBean> courseBeans = getCourseService().getActiveCourses();
            model.addAttribute("courses", courseBeans);
        }catch (Exception ex){
            LOG.error("Error in Course Controller", ex);
        }

        try{
            List<FacultyBean> list = getFacultyService().getActiveFaculties();
            model.addAttribute("faculties", list);
        }catch (Exception ex){
            LOG.error("Error in Faculty Controller ", ex);
        }
    }
}
