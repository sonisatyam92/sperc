package org.sperc.controller.download;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sperc.controller.AbstractController;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Satyam Soni on 5/10/2015.
 */
@Controller
public class DownloadController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(DownloadController.class);

    @RequestMapping(value = "/downloadAdmissionForm", method = RequestMethod.GET, produces = "application/pdf")
    public void downloadAdmissionForm(HttpServletRequest request, HttpServletResponse response){
        try{

            InputStream in  = this.getClass().getResourceAsStream("/pdf/admissionForm.pdf");
            int i = 0;
            StringBuilder str=new StringBuilder();
            while((i = in.read()) != -1){
                str.append((char)i);
            }

            in.close();
            System.out.println(str);

            streamReport(response, str.toString().getBytes(), "AppForm");




        }catch (Exception ex){
            LOG.error(ex.getMessage(), ex);

        }
    }



    protected void streamReport(HttpServletResponse response, byte[] data, String name)
            throws IOException {

        response.setContentType("application/octet-stream");
        response.setHeader("Content-disposition", "attachment; filename=" + name);
        response.setContentLength(data.length);

        response.getOutputStream().write(data);
        response.getOutputStream().flush();
    }


}
