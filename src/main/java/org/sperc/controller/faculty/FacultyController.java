package org.sperc.controller.faculty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sperc.bean.FacultyBean;
import org.sperc.controller.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Satyam Soni on 4/12/2015.
 */
@Controller
public class FacultyController extends AbstractController {
        private static final Logger LOG = LoggerFactory.getLogger(FacultyController.class);

    @RequestMapping(value = "/faculties")
        public ModelAndView faculty(Model model, HttpServletRequest request, HttpServletResponse response){

          populateCommonModel(model);
            return new ModelAndView("faculty");

        }
}
