package org.sperc.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sperc.bean.ConfigBean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Satyam Soni on 4/11/2015.
 */
@Component(value = "configDAO")
public class ConfigDAO extends AbstractBaseDAO {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigDAO.class);

    public Map<String,String> getConfigMap() throws Exception {
        try{
            List<ConfigBean> beans = getCurrentSession().createQuery("from ConfigBean").list();
            Map<String,String> map = new HashMap<String, String>();
            for(final ConfigBean c : beans){
                map.put(c.getName(), c.getValue());
            }
            return map;
        }catch (Exception ex){
            LOG.error("Exception in ConfigDAOs", ex);
            throw ex;
        }
    }
}
