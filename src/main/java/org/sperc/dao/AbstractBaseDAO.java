package org.sperc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Satyam Soni on 4/11/2015.
 */
public abstract class AbstractBaseDAO implements BaseDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public final void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected final Session getCurrentSession(){
       return getSessionFactory().getCurrentSession();
    }

    protected final Session getNewSession(){
        return getSessionFactory().openSession();
    }
}
