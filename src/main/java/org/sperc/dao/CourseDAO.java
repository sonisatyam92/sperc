package org.sperc.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sperc.bean.CourseBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Satyam Soni on 4/11/2015.
 */
@Component(value = "courseDAO")
public class CourseDAO extends AbstractBaseDAO {
    private static final Logger LOG = LoggerFactory.getLogger(CourseDAO.class);

    public List<CourseBean> getActiveCourses() throws  Exception{
        try{
            return getCurrentSession().createQuery("from CourseBean where active = true").list();
        }catch (Exception ex){
            LOG.error("Error in Course DAO", ex);
            throw ex;
        }
    }

    public List<CourseBean> getAllCourses() throws  Exception{
        try{
            return getCurrentSession().createQuery("from CourseBean").list();
        }catch (Exception ex){
            LOG.error("Error in Course DAO", ex);
            throw ex;
        }
    }

    public List<CourseBean> getInactiveCourses() throws  Exception{
        try{
            return getCurrentSession().createQuery("from CourseBean where active = false").list();
        }catch (Exception ex){
            LOG.error("Error in Course DAO", ex);
            throw ex;
        }
    }

    public CourseBean updateCourse(CourseBean courseBean) throws  Exception{
        try{
             getCurrentSession().update(courseBean);
            return courseBean;
        }catch (Exception ex){
            LOG.error("Error in Course DAO", ex);
            throw ex;
        }
    }

    public CourseBean addCourse(CourseBean courseBean) throws  Exception{
        try{
            getCurrentSession().saveOrUpdate(courseBean);
            return courseBean;
        }catch (Exception ex){
            LOG.error("Error in Course DAO", ex);
            throw ex;
        }
    }


    public CourseBean activateDeactivateCourse(boolean activate, CourseBean courseBean) throws  Exception{
        try{
            getCurrentSession().createQuery("update CourseBean set active = :active where id = :id")
                    .setParameter("active", activate)
                    .setParameter("id", courseBean.getId());
            return courseBean;
        }catch (Exception ex){
            LOG.error("Error in Course DAO", ex);
            throw ex;
        }
    }

    public Integer activateDeactivateCourse(boolean activate, Integer id) throws  Exception{
        try{
            getCurrentSession().createQuery("update CourseBean set active = :active where id = :id")
                    .setParameter("active", activate)
                    .setParameter("id", id);
            return id;
        }catch (Exception ex){
            LOG.error("Error in Course DAO", ex);
            throw ex;
        }
    }
}
