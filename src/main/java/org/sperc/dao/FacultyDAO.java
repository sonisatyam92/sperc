package org.sperc.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sperc.bean.FacultyBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Satyam Soni on 4/12/2015.
 */
@Component(value = "facultyDAO")
public class FacultyDAO  extends AbstractBaseDAO {

    private static final Logger LOG = LoggerFactory.getLogger(FacultyDAO.class);

    public List<FacultyBean> getActiveFaculties() throws Exception {
        try{
            return getCurrentSession().createQuery("from FacultyBean  where active = true").list();
        }catch (Exception ex){
            LOG.error("Error in FacultDAO" , ex);
            throw ex;
        }
    }

    public List<FacultyBean> getAllFaculties() throws Exception {
        try{
            return getCurrentSession().createQuery("from FacultyBean").list();
        }catch (Exception ex){
            LOG.error("Error in FacultDAO" , ex);
            throw ex;
        }
    }

    public List<FacultyBean> getInactiveFaculties() throws Exception {
        try{
            return getCurrentSession().createQuery("from FacultyBean  where active = false").list();
        }catch (Exception ex){
            LOG.error("Error in FacultDAO" , ex);
            throw ex;
        }
    }

    public FacultyBean updateFaculty(FacultyBean facultyBean) throws Exception {
        try{
            getCurrentSession().saveOrUpdate(facultyBean);
            return facultyBean;
        }catch (Exception ex){
            LOG.error("Error in FacultDAO" , ex);
            throw ex;
        }
    }


    public FacultyBean addFaculty(FacultyBean bean) throws Exception {
        try{
            getCurrentSession().saveOrUpdate(bean);
            return bean;
        }catch (Exception ex){
            LOG.error("Error in FacultDAO" , ex);
            throw ex;
        }
    }
}
