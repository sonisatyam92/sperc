package org.sperc.common;

import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by Satyam Soni on 4/11/2015.
 */

public class AppConstants {

    public static Map<String,String> CONFIG_MAP = null;

    public static void resetConfigMap(){
        synchronized (CONFIG_MAP){
            CONFIG_MAP = null;
        }
    }

}
