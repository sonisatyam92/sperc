package org.sperc.common;

/**
 * Created by Satyam Soni on 4/11/2015.
 */
public enum AppEnum {
    CONTACT_MAIL("contactmail");

    private String value;
    AppEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
