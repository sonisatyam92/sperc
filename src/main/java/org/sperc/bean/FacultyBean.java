package org.sperc.bean;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Satyam Soni on 4/12/2015.
 */
@Entity
@Table(name = "faculty")
public class FacultyBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "faculty_id")
    private Integer id;


    @Column(name = "faculty_name")
    private String name;

    @Column(name = "faculty_image_url")
    private String url;

    @Column(name="faculty_designation")
    private String designation;

    @Column(name = "faculty_email")
    private String email;

    @Column(name = "faculty_ph")
    private String ph;

    @Column(name = "faculty_sub_title")
    private String subTitle;

    @Column(name = "faculty_description")
    private String description;

    @Column(name = "active")
    private Boolean active;

    /*@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private UserBean createdBy;*/


    @Column(name = "created_by")
    private String createdBy;


    @Column(name = "created_date")
    private Date createdDate;

    /*@OneToOne
    @JoinColumn(name = "updated_by")
    private UserBean updatedBy;*/

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name= "updated_date")
    private Date updatedDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPh() {
        return ph;
    }

    public void setPh(String ph) {
        this.ph = ph;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    /*public UserBean getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserBean createdBy) {
        this.createdBy = createdBy;
    }*/

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /*public UserBean getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(UserBean updatedBy) {
        this.updatedBy = updatedBy;
    }*/

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
}
