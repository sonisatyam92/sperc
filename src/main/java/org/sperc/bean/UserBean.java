package org.sperc.bean;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Satyam Soni on 4/12/2015.
 */
@Entity
@Table(name = "user")
public class UserBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_code")
    private String userCode;

    @Column(name = "email")
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "gender")
    private Integer gender;

    @Column(name = "ph_no")
    private String phNo;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "password")
    private String password;

    @Column(name = "reset")
    private Integer reset;


    @Column(name = "created_date")
    private Date createdDate;

    @OneToOne
    @JoinColumn(name = "created_by")
    private UserBean createdBy;


    @Column(name = "updated_date")
    private Date updatedDate;

    @OneToOne
    @JoinColumn(name = "updated_by")
    private UserBean updatedBy;

    @Column(name = "updated_pwd")
    private Date updatedPwd;

    @Column(name = "access_level")
    private Integer accessLevel;

    @Column(name = "approved")
    private Boolean approved;

    @OneToOne
    @JoinColumn(name = "approved_by")
    private UserBean approvedBy;


    @Column(name = "approved_date")
    private Date approvedDate;

    @Column(name = "enroll_no")
    private String enrollNo;

    @Column(name = "last_login_date")
    private Date lastLoginDate;

    @Column(name = "is_login")
    private Boolean isLogin;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getPhNo() {
        return phNo;
    }

    public void setPhNo(String phNo) {
        this.phNo = phNo;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getReset() {
        return reset;
    }

    public void setReset(Integer reset) {
        this.reset = reset;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public UserBean getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserBean createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedPwd() {
        return updatedPwd;
    }

    public void setUpdatedPwd(Date updatedPwd) {
        this.updatedPwd = updatedPwd;
    }

    public Integer getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(Integer accessLevel) {
        this.accessLevel = accessLevel;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public UserBean getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(UserBean approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getEnrollNo() {
        return enrollNo;
    }

    public void setEnrollNo(String enrollNo) {
        this.enrollNo = enrollNo;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Boolean getIsLogin() {
        return isLogin;
    }

    public void setIsLogin(Boolean isLogin) {
        this.isLogin = isLogin;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public UserBean getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(UserBean updatedBy) {
        this.updatedBy = updatedBy;
    }
}
