package org.sperc.service;

import org.sperc.bean.FacultyBean;
import org.sperc.dao.FacultyDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Satyam Soni on 4/12/2015.
 */
@Service
@Transactional
public class FacultyService extends AbstractBaseService {
    @Autowired
    private FacultyDAO facultyDAO;

    public FacultyDAO getFacultyDAO() {
        return facultyDAO;
    }

    public void setFacultyDAO(FacultyDAO facultyDAO) {
        this.facultyDAO = facultyDAO;
    }

    public List<FacultyBean> getActiveFaculties() throws Exception {
        return getFacultyDAO().getActiveFaculties();
    }

    public List<FacultyBean> getAllFaculties() throws Exception {
        return getFacultyDAO().getAllFaculties();    }

    public List<FacultyBean> getInactiveFaculties() throws Exception {
        return getFacultyDAO().getInactiveFaculties();
    }

    public FacultyBean updateFaculty(FacultyBean facultyBean) throws Exception {
        return getFacultyDAO().updateFaculty(facultyBean);
    }


    public FacultyBean addFaculty(FacultyBean bean) throws Exception {
        return getFacultyDAO().addFaculty(bean);
    }
}
