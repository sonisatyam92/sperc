package org.sperc.service;

import org.sperc.dao.ConfigDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * Created by Satyam Soni on 4/11/2015.
 */
@Service(value = "configService")
@Transactional
public class ConfigService extends AbstractBaseService {

    @Autowired
    private ConfigDAO configDAO;

    public ConfigDAO getConfigDAO() {
        return configDAO;
    }

    public final void setConfigDAO(ConfigDAO configDAO) {
        this.configDAO = configDAO;
    }

    public Map<String,String> getConfigMap() throws Exception {
       return getConfigDAO().getConfigMap();
    }
}
