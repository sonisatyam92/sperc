package org.sperc.service;

import org.sperc.bean.CourseBean;
import org.sperc.dao.CourseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Satyam Soni on 4/12/2015.
 */
@Service(value = "courseService")
@Transactional
public class CourseService extends AbstractBaseService {

        @Autowired
    private CourseDAO courseDAO;

    public CourseDAO getCourseDAO() {
        return courseDAO;
    }

    public void setCourseDAO(CourseDAO courseDAO) {
        this.courseDAO = courseDAO;
    }




    public List<CourseBean> getActiveCourses() throws  Exception{
        return getCourseDAO().getActiveCourses();
    }

    public List<CourseBean> getAllCourses() throws  Exception{
        return getCourseDAO().getAllCourses();
    }

    public List<CourseBean> getInactiveCourses() throws  Exception{
        return getCourseDAO().getInactiveCourses();
    }

    public CourseBean updateCourse(CourseBean courseBean) throws  Exception{
      return getCourseDAO().updateCourse(courseBean);
    }

    public CourseBean addCourse(CourseBean courseBean) throws  Exception{
        return getCourseDAO().addCourse(courseBean);
    }


    public CourseBean activateDeactivateCourse(boolean activate, CourseBean courseBean) throws  Exception{
        return getCourseDAO().activateDeactivateCourse(activate, courseBean);
    }

    public Integer activateDeactivateCourse(boolean activate, Integer id) throws  Exception{
       return getCourseDAO().activateDeactivateCourse(activate, id);
    }
}
