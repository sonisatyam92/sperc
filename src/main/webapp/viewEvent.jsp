<%@ page import="org.sperc.bean.CourseBean" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BUNTINGTON Public Schools</title>

    <!-- Styles -->
    <link href="css.css" rel="stylesheet" type="text/css"><!-- Google web fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- font-awesome -->
    <link href="js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css"><!-- dropdown-menu -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- Bootstrap -->
    <link href="js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"><!-- Fancybox -->
    <link href="js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css"><!-- Audioplayer -->
    <link href="style.css" rel="stylesheet" type="text/css"><!-- theme styles -->

</head>

<body role="document" class="single-event">

<!-- device test, don't remove. javascript needed! -->
<span class="visible-xs"></span><span class="visible-sm"></span><span class="visible-md"></span><span class="visible-lg"></span>
<!-- device test end -->

<jsp:include page="header.jsp"/>
<div id="k-body"><!-- content wrapper -->

<div class="container"><!-- container -->

<div class="row"><!-- row -->

    <div id="k-top-search" class="col-lg-12 clearfix"><!-- top search -->

        <form action="events-single.html#" id="top-searchform" method="get" role="search">
            <div class="input-group">
                <input type="text" name="s" id="sitesearch" class="form-control" autocomplete="off" placeholder="Type in keyword(s) then hit Enter on keyboard" />
            </div>
        </form>

        <div id="bt-toggle-search" class="search-icon text-center"><i class="s-open fa fa-search"></i><i class="s-close fa fa-times"></i></div><!-- toggle search button -->

    </div><!-- top search end -->

    <div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->

        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="events.html">Events</a></li>
            <li class="active">Annual Alumni Game</li>
        </ol>

    </div><!-- breadcrumbs end -->

</div><!-- row end -->

<div class="row no-gutter"><!-- row -->

    <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

        <div class="col-padded"><!-- inner custom column -->

            <div class="row gutter"><!-- row -->

                <div class="col-lg-12 col-md-12">

                    <div class="events-title-meta clearfix">
                        <h1 class="page-title">Annual Alumni Game</h1>
                        <div class="event-meta">
                            <span class="event-from">Jun 12, 2014</span>
                            <span class="event-divider">to</span>
                            <span class="event-to">Jun 15, 2014</span>
                            <span class="event-time">16:00 - 21:00</span>
                        </div>
                    </div>

                    <div class="news-body clearfix">
                        <p>
                            Duis ornare magna sit amet dui eleifend imperdiet. Aliquam at porta elit. Proin lorem lacus, tempus id diam sit amet, <a href="events-single.html#" title="porttitor tempor">porttitor tempor</a> lectus. Praesent id felis sagittis, suscipit ligula sed, condimentum nisi. In non commodo risus. Praesent fringilla ligula in orci consectetur pulvinar. Nunc <strong>facilisis metus pellentesque</strong>, vestibulum libero eget, varius elit. Aliquam sed gravida dui, a imperdiet eros. Cras dignissim libero id feugiat pharetra. Nullam ut bibendum est, sed tincidunt massa.
                        </p>

                        <hr />

                        <h6>Programme</h6>
                        <p>
                            Aenean eu sem non diam iaculis gravida. Nam imperdiet eleifend <strong>erat ut viverra</strong>. Fusce vitae ultrices tortor, tincidunt consectetur purus. Donec ut suscipit nisi. Ut vitae venenatis dolor. Phasellus volutpat sapien sit amet tellus gravida, nec dignissim elit congue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo magna, condimentum vitae feugiat id, lobortis et nibh.
                        </p>

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Event</th>
                                <th>Schedule</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>consectetur pulvinar</td>
                                <td>16:00 - 17:00</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>metus pellentesque</td>
                                <td>17:15 - 17:30</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>varius elit</td>
                                <td>17:30 - 18:00</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>porttitor tempor</td>
                                <td>18:00 - 20:00</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>suscipit ligula</td>
                                <td>20:00 - 21:00</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                </div>

            </div><!-- row end -->

        </div><!-- inner custom column end -->

    </div><!-- doc body wrapper end -->

    <div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->

        <div class="col-padded col-shaded"><!-- inner custom column -->

            <ul class="list-unstyled clear-margins"><!-- widgets -->

                <li class="widget-container widget_nav_menu"><!-- widget -->

                    <h1 class="title-widget">Useful links</h1>

                    <ul>
                        <li><a href="events-single.html#" title="menu item">News Archive</a></li>
                        <li><a href="events-single.html#" title="menu item">Tradition of School Events</a></li>
                        <li><a href="events-single.html#" title="menu item">Report Asocial Behaviour</a></li>
                        <li><a href="events-single.html#" title="menu item">Trends and Tips</a></li>
                        <li><a href="events-single.html#" title="menu item">Events Poll</a></li>
                    </ul>

                </li>

                <li class="widget-container widget_recent_news"><!-- widgets list -->

                    <h1 class="title-widget">School News</h1>

                    <ul class="list-unstyled">

                        <li class="recent-news-wrap news-no-summary">

                            <div class="recent-news-content clearfix">
                                <figure class="recent-news-thumb">
                                    <a href="events-single.html#" title="Megan Boyle flourishes..."><img src="img/recent-news-thumb-1.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 1" /></a>
                                </figure>
                                <div class="recent-news-text">
                                    <div class="recent-news-meta">
                                        <div class="recent-news-date">Jun 12, 2014</div>
                                    </div>
                                    <h1 class="title-median"><a href="events-single.html#" title="Megan Boyle flourishes...">Megan Boyle flourishes at Boston University</a></h1>
                                </div>
                            </div>

                        </li>

                        <li class="recent-news-wrap news-no-summary">

                            <div class="recent-news-content clearfix">
                                <figure class="recent-news-thumb">
                                    <a href="events-single.html#" title="Buntington Alum..."><img src="img/recent-news-thumb-2.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 2" /></a>
                                </figure>
                                <div class="recent-news-text">
                                    <div class="recent-news-meta">
                                        <div class="recent-news-date">Jun 10, 2014</div>
                                    </div>
                                    <h1 class="title-median"><a href="events-single.html#" title="Buntington Alum...">Buntington Alum Marc Bloom Pens New Book</a></h1>
                                </div>
                            </div>

                        </li>

                        <li class="recent-news-wrap news-no-summary">

                            <div class="recent-news-content clearfix">
                                <figure class="recent-news-thumb">
                                    <a href="events-single.html#" title="Cody Rotschild Enjoys..."><img src="img/recent-news-thumb-3.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 3" /></a>
                                </figure>
                                <div class="recent-news-text">
                                    <div class="recent-news-meta">
                                        <div class="recent-news-date">Jun 05, 2014</div>
                                    </div>
                                    <h1 class="title-median"><a href="events-single.html#" title="Cody Rotschild Enjoys...">Cody Rotschild Enjoys Life in Montreal</a></h1>
                                </div>
                            </div>

                        </li>

                    </ul>

                </li><!-- widgets list end -->

                <li class="widget-container widget_newsletter"><!-- widget -->

                    <h1 class="title-titan">School Newsletter</h1>

                    <form role="search" method="get" class="newsletter-form" action="events-single.html#">
                        <div class="input-group">
                            <input type="text" placeholder="Your e-mail address" autocomplete="off" class="form-control newsletter-form-input" name="email" />
                            <span class="input-group-btn"><button type="submit" class="btn btn-default">GO!</button></span>
                        </div>
                        <span class="help-block">* Enter your e-mail address to subscribe.</span>
                    </form>

                </li>

                <li class="widget-container widget_text"><!-- widget -->

                    <a href="events-single.html#" class="custom-button cb-red" title="How to apply?">
                        <i class="custom-button-icon fa fa-empire"></i>
                                    <span class="custom-button-wrap">
                                    	<span class="custom-button-title">Donate Now</span>
                                        <span class="custom-button-tagline">Become a corporate sponsor of our schools!</span>
                                    </span>
                        <em></em>
                    </a>

                </li>

            </ul><!-- widgets end -->

        </div><!-- inner custom column end -->

    </div><!-- sidebar wrapper end -->

</div><!-- row end -->

</div><!-- container end -->

</div><!-- content wrapper end -->

<jsp:include page="footer.jsp"/>
<!-- jQuery -->
<script src="jQuery/jquery-2.1.1.min.js"></script>
<script src="jQuery/jquery-migrate-1.2.1.min.js"></script>

<!-- Bootstrap -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<!-- Drop-down -->
<script src="js/dropdown-menu/dropdown-menu.js"></script>

<!-- Fancybox -->
<script src="js/fancybox/jquery.fancybox.pack.js"></script>
<script src="js/fancybox/jquery.fancybox-media.js"></script><!-- Fancybox media -->

<!-- Responsive videos -->
<script src="js/jquery.fitvids.js"></script>

<!-- Audio player -->
<script src="js/audioplayer/audioplayer.min.js"></script>

<!-- Pie charts -->
<script src="js/jquery.easy-pie-chart.js"></script>

<!-- Google Maps -->
<script src="maps/api/js.JS"></script>

<!-- Theme -->
<script src="js/theme.js"></script>

</body>
</html>