<%@ page import="org.sperc.bean.FacultyBean" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BUNTINGTON Public Schools</title>

    <!-- Styles -->
    <link href="css.css" rel="stylesheet" type="text/css"><!-- Google web fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- font-awesome -->
    <link href="js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css"><!-- dropdown-menu -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- Bootstrap -->
    <link href="js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"><!-- Fancybox -->
    <link href="js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css"><!-- Audioplayer -->
    <link href="style.css" rel="stylesheet" type="text/css"><!-- theme styles -->

</head>

<body role="document">

<!-- device test, don't remove. javascript needed! -->
<span class="visible-xs"></span><span class="visible-sm"></span><span class="visible-md"></span><span class="visible-lg"></span>
<!-- device test end -->

<jsp:include page="header.jsp"/>

<div id="k-body"><!-- content wrapper -->

<div class="container"><!-- container -->

<div class="row"><!-- row -->



    <div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->

        <ol class="breadcrumb">
            <li><a href="home">Home</a></li>
            <li><a href="aboutus">About Us</a></li>
            <li class="active">Faculty</li>
        </ol>

    </div><!-- breadcrumbs end -->

</div><!-- row end -->

<div class="row no-gutter"><!-- row -->

<div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

    <div class="col-padded"><!-- inner custom column -->

        <div class="row gutter"><!-- row -->

            <div class="col-lg-12 col-md-12">

                <h1 class="page-title">Faculties</h1><!-- category title -->

            </div>

        </div><!-- row end -->

        <div class="row gutter"><!-- row -->

            <div class="col-lg-12 col-md-12">
                <%
                    List<FacultyBean> list = (List<FacultyBean>) request.getAttribute("faculties");
                    for(FacultyBean f : list){
                    %>

                <div class="leadership-wrapper"><!-- leadership single wrap -->

                    <figure class="leadership-photo">
                        <img src='<%=f.getUrl()%>' alt='<%=f.getName() + " ,"+ f.getDesignation()%>' />
                    </figure>

                    <div class="leadership-meta clearfix">
                        <h1 class="leadership-function title-median"><%=f.getName()%>, <small><%=f.getDesignation()%></small></h1>
                        <div class="leadership-position"><%=f.getEmail()%></div>
                        <div class="leadership-position"><%=f.getPh()%></div>
                        <div class="leadership-position"><%=f.getSubTitle()%></div>
                        <p class="leadership-bio">
                            <%=f.getDescription()%>
                        </p>
                    </div>

                </div><!-- leadership single wrap end -->

                <%
                    }
                %>


               <%-- <div class="leadership-wrapper"><!-- leadership single wrap -->

                    <figure class="leadership-photo">
                        <img src="img/leadership-2.jpg" alt="Dr. Coraline Breaston, Assistant Superintendent" />
                    </figure>

                    <div class="leadership-meta clearfix">
                        <h1 class="leadership-function title-median">Dr. Coraline Breaston, <small>Assistant Superintendent</small></h1>
                        <div class="leadership-position">Administrative team member since February 2012</div>
                        <p class="leadership-bio">
                            Morbi molestie molestie libero eu tempus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam cursus lacinia sodales. Nullam suscipit egestas tortor. Aenean convallis eu quam nec suscipit. Phasellus ut ullamcorper sapien. Duis vehicula nec eros vel vehicula.
                        </p>
                    </div>

                </div><!-- leadership single wrap end -->

                <div class="leadership-wrapper"><!-- leadership single wrap -->

                    <figure class="leadership-photo">
                        <img src="img/leadership-3.jpg" alt="Dr. Dominic Purcell, Assistant Superintendent" />
                    </figure>

                    <div class="leadership-meta clearfix">
                        <h1 class="leadership-function title-median">Dr. Dominic Purcell, <small>Assistant Superintendent</small></h1>
                        <div class="leadership-position">Administrative team member since March 2012</div>
                        <p class="leadership-bio">
                            Pellentesque rutrum iaculis dui eu facilisis. Etiam tincidunt semper ante, at accumsan ipsum sodales eleifend. Nunc aliquam erat id nulla ultrices varius. Cras luctus laoreet leo quis porttitor. Donec tempus dui a elit ullamcorper, elementum fermentum purus consectetur. Sed adipiscing facilisis mollis.
                        </p>
                    </div>

                </div><!-- leadership single wrap end -->

                <div class="leadership-wrapper"><!-- leadership single wrap -->

                    <figure class="leadership-photo">
                        <img src="img/leadership-4.jpg" alt="Simpson Weiss, Public Relations" />
                    </figure>

                    <div class="leadership-meta clearfix">
                        <h1 class="leadership-function title-median">Simpson Weiss, <small>Public Relations</small></h1>
                        <div class="leadership-position">Administrative team member since May 2004</div>
                        <p class="leadership-bio">
                            Aliquam quis laoreet purus. Donec neque elit, vestibulum eget augue tempus, iaculis ultricies quam. Nulla imperdiet, leo non suscipit vestibulum, eros dolor pharetra quam, imperdiet tempus tellus sapien ut velit. Nam nec lobortis sem. In tincidunt dolor at fermentum varius. Suspendisse volutpat libero id tortor volutpat, sit amet pharetra nisl tempor. Suspendisse sit amet tristique ante.
                        </p>
                    </div>

                </div><!-- leadership single wrap end -->

--%>            </div>

        </div><!-- row end -->

    </div><!-- inner custom column end -->

</div><!-- doc body wrapper end -->

<div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->

    <div class="col-padded col-shaded"><!-- inner custom column -->

        <ul class="list-unstyled clear-margins"><!-- widgets -->

            <%--<li class="widget-container widget_nav_menu"><!-- widget -->

                <h1 class="title-widget">Useful links</h1>

                <ul>
                    <li><a href="school-leadership.html#" title="menu item">News Archive</a></li>
                    <li><a href="school-leadership.html#" title="menu item">Tradition of School Events</a></li>
                    <li><a href="school-leadership.html#" title="menu item">Report Asocial Behaviour</a></li>
                    <li><a href="school-leadership.html#" title="menu item">Trends and Tips</a></li>
                    <li><a href="school-leadership.html#" title="menu item">Events Poll</a></li>
                </ul>

            </li>

            <li class="widget-container widget_recent_news"><!-- widgets list -->

                <h1 class="title-widget">School News</h1>

                <ul class="list-unstyled">

                    <li class="recent-news-wrap news-no-summary">

                        <div class="recent-news-content clearfix">
                            <figure class="recent-news-thumb">
                                <a href="school-leadership.html#" title="Megan Boyle flourishes..."><img src="img/recent-news-thumb-1.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 1" /></a>
                            </figure>
                            <div class="recent-news-text">
                                <div class="recent-news-meta">
                                    <div class="recent-news-date">Jun 12, 2014</div>
                                </div>
                                <h1 class="title-median"><a href="school-leadership.html#" title="Megan Boyle flourishes...">Megan Boyle flourishes at Boston University</a></h1>
                            </div>
                        </div>

                    </li>

                    <li class="recent-news-wrap news-no-summary">

                        <div class="recent-news-content clearfix">
                            <figure class="recent-news-thumb">
                                <a href="school-leadership.html#" title="Buntington Alum..."><img src="img/recent-news-thumb-2.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 2" /></a>
                            </figure>
                            <div class="recent-news-text">
                                <div class="recent-news-meta">
                                    <div class="recent-news-date">Jun 10, 2014</div>
                                </div>
                                <h1 class="title-median"><a href="school-leadership.html#" title="Buntington Alum...">Buntington Alum Marc Bloom Pens New Book</a></h1>
                            </div>
                        </div>

                    </li>

                    <li class="recent-news-wrap news-no-summary">

                        <div class="recent-news-content clearfix">
                            <figure class="recent-news-thumb">
                                <a href="school-leadership.html#" title="Cody Rotschild Enjoys..."><img src="img/recent-news-thumb-3.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 3" /></a>
                            </figure>
                            <div class="recent-news-text">
                                <div class="recent-news-meta">
                                    <div class="recent-news-date">Jun 05, 2014</div>
                                </div>
                                <h1 class="title-median"><a href="school-leadership.html#" title="Cody Rotschild Enjoys...">Cody Rotschild Enjoys Life in Montreal</a></h1>
                            </div>
                        </div>

                    </li>

                </ul>

            </li><!-- widgets list end -->--%>

            <li class="widget-container widget_newsletter"><!-- widget -->

                <h1 class="title-titan">School Newsletter</h1>

                <form role="search" method="get" class="newsletter-form" action="school-leadership.html#">
                    <div class="input-group">
                        <input type="text" placeholder="Your e-mail address" autocomplete="off" class="form-control newsletter-form-input" name="email" />
                        <span class="input-group-btn"><button type="submit" class="btn btn-default">GO!</button></span>
                    </div>
                    <span class="help-block">* Enter your e-mail address to subscribe.</span>
                </form>

            </li>

            <li class="widget-container widget_text"><!-- widget -->

                <a href="school-leadership.html#" class="custom-button cb-red" title="How to apply?">
                    <i class="custom-button-icon fa fa-empire"></i>
                                    <span class="custom-button-wrap">
                                    	<span class="custom-button-title">Donate Now</span>
                                        <span class="custom-button-tagline">Become a corporate sponsor of our schools!</span>
                                    </span>
                    <em></em>
                </a>

            </li>

        </ul><!-- widgets end -->

    </div><!-- inner custom column end -->

</div><!-- sidebar wrapper end -->

</div><!-- row end -->

</div><!-- container end -->

</div><!-- content wrapper end -->

<jsp:include page="footer.jsp"/>

<!-- jQuery -->
<script src="jQuery/jquery-2.1.1.min.js"></script>
<script src="jQuery/jquery-migrate-1.2.1.min.js"></script>

<!-- Bootstrap -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<!-- Drop-down -->
<script src="js/dropdown-menu/dropdown-menu.js"></script>

<!-- Fancybox -->
<script src="js/fancybox/jquery.fancybox.pack.js"></script>
<script src="js/fancybox/jquery.fancybox-media.js"></script><!-- Fancybox media -->

<!-- Responsive videos -->
<script src="js/jquery.fitvids.js"></script>

<!-- Audio player -->
<script src="js/audioplayer/audioplayer.min.js"></script>

<!-- Pie charts -->
<script src="js/jquery.easy-pie-chart.js"></script>

<!-- Google Maps -->
<script src="maps/api/js.JS"></script>

<!-- Theme -->
<script src="js/theme.js"></script>

</body>
</html>