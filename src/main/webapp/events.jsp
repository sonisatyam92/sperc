<%@ page import="org.sperc.bean.CourseBean" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BUNTINGTON Public Schools</title>

    <!-- Styles -->
    <link href="css.css" rel="stylesheet" type="text/css"><!-- Google web fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- font-awesome -->
    <link href="js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css"><!-- dropdown-menu -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- Bootstrap -->
    <link href="js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"><!-- Fancybox -->
    <link href="js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css"><!-- Audioplayer -->
    <link href="style.css" rel="stylesheet" type="text/css"><!-- theme styles -->

</head>

<body role="document">

<!-- device test, don't remove. javascript needed! -->
<span class="visible-xs"></span><span class="visible-sm"></span><span class="visible-md"></span><span class="visible-lg"></span>
<!-- device test end -->

<jsp:include page="header.jsp"/>

<div id="k-body"><!-- content wrapper -->

<div class="container"><!-- container -->

<div class="row"><!-- row -->

    <div id="k-top-search" class="col-lg-12 clearfix"><!-- top search -->

        <form action="events.html#" id="top-searchform" method="get" role="search">
            <div class="input-group">
                <input type="text" name="s" id="sitesearch" class="form-control" autocomplete="off" placeholder="Type in keyword(s) then hit Enter on keyboard" />
            </div>
        </form>

        <div id="bt-toggle-search" class="search-icon text-center"><i class="s-open fa fa-search"></i><i class="s-close fa fa-times"></i></div><!-- toggle search button -->

    </div><!-- top search end -->

    <div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->

        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li class="active">Events</li>
        </ol>

    </div><!-- breadcrumbs end -->

</div><!-- row end -->

<div class="row no-gutter"><!-- row -->

<div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

    <div class="col-padded"><!-- inner custom column -->

        <div class="row gutter"><!-- row -->

            <div class="col-lg-12 col-md-12">

                <h1 class="page-title">Upcoming Events</h1><!-- category title -->

            </div>

        </div><!-- row end -->

        <div class="row gutter"><!-- row -->

            <div class="col-lg-12 col-md-12">

                <div class="up-event-wrapper"><!-- event summary -->

                    <h1 class="title-median"><a href="events.html#" title="Annual alumni game">Annual alumni game</a></h1>

                    <div class="up-event-meta clearfix">
                        <div class="up-event-date">Jul 25, 2015</div><div class="up-event-time">9:00 - 11:00</div>
                    </div>

                    <p>
                        Suspendisse faucibus sem nec dictum tristique. Morbi quis scelerisque ligula. Mauris id nisl eget velit tincidunt congue. Nam vel egestas arcu. Donec pretium rutrum justo eu dapibus... <a href="events.html#" class="moretag" title="read more">MORE</a>
                    </p>

                </div><!-- event summary end -->

                <div class="up-event-wrapper"><!-- event summary -->

                    <h1 class="title-median"><a href="events.html#" title="School talents gathering">School talents gathering</a></h1>

                    <div class="up-event-meta clearfix">
                        <div class="up-event-date">Aug 25, 2015</div><div class="up-event-time">8:30 - 10:30</div>
                    </div>

                    <p>
                        Curabitur fringilla, massa sit amet ultricies iaculis, nisi lorem faucibus justo, eu interdum odio nunc eu lorem. Aliquam auctor id augue posuere consectetur. Maecenas mollis nisl ut diam condimentum, eu sagittis ante elementum... <a href="events.html#" class="moretag" title="read more">MORE</a>
                    </p>

                </div><!-- event summary end -->

                <div class="up-event-wrapper"><!-- event summary -->

                    <h1 class="title-median"><a href="events.html#" title="Campus">Campus "Open Doors"</a></h1>

                    <div class="up-event-meta clearfix">
                        <div class="up-event-date">Sep 04, 2015</div><div class="up-event-date">Sep 11, 2015</div>
                    </div>

                    <p>
                        In non commodo risus. Praesent fringilla ligula in orci consectetur pulvinar. Nunc facilisis metus pellentesque, vestibulum libero eget, varius elit. Aliquam sed gravida dui, a imperdiet eros... <a href="events.html#" class="moretag" title="read more">MORE</a>
                    </p>

                </div><!-- event summary end -->

                <div class="up-event-wrapper"><!-- event summary -->

                    <h1 class="title-median"><a href="events.html#" title="Brian McMeltingberg...">Brian McMeltingberg Anniversary</a></h1>

                    <div class="up-event-meta clearfix">
                        <div class="up-event-date">Sep 15, 2015</div><div class="up-event-time">13:30 - 15:00</div>
                    </div>

                    <p>
                        Phasellus volutpat sapien sit amet tellus gravida, nec dignissim elit congue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo magna, condimentum vitae feugiat id, lobortis et nibh... <a href="events.html#" class="moretag" title="read more">MORE</a>
                    </p>

                </div><!-- event summary end -->

                <div class="up-event-wrapper"><!-- event summary -->

                    <h1 class="title-median"><a href="events.html#" title="Newcomers...">Newcomers welcome party</a></h1>

                    <div class="up-event-meta clearfix">
                        <div class="up-event-date">Sep 23, 2015</div><div class="up-event-time">19:30 - 02:00</div>
                    </div>

                    <p>
                        Sed magna nisl, consectetur sed nisl in, fermentum bibendum dui. Suspendisse vulputate diam quis tellus accumsan, et pellentesque massa iaculis. Nullam hendrerit lacinia nisl, at tincidunt orci dictum in... <a href="events.html#" class="moretag" title="read more">MORE</a>
                    </p>

                </div><!-- event summary end -->

            </div>

        </div><!-- row end -->

        <div class="row gutter"><!-- row -->

            <div class="col-lg-12">

                <ul class="pagination pull-right"><!-- pagination -->
                    <li class="disabled"><a href="events.html#">Prev</a></li>
                    <li class="active"><a href="events.html#">1</a></li>
                    <li><a href="events.html#">2</a></li>
                    <li><a href="events.html#">3</a></li>
                    <li><a href="events.html#">4</a></li>
                    <li><a href="events.html#">5</a></li>
                    <li><a href="events.html#">Next</a></li>
                </ul><!-- pagination end -->

            </div>

        </div><!-- row end -->

    </div><!-- inner custom column end -->

</div><!-- doc body wrapper end -->

<div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->

    <div class="col-padded col-shaded"><!-- inner custom column -->

        <ul class="list-unstyled clear-margins"><!-- widgets -->

            <li class="widget-container widget_nav_menu"><!-- widget -->

                <h1 class="title-widget">Useful links</h1>

                <ul>
                    <li><a href="events.html#" title="menu item">News Archive</a></li>
                    <li><a href="events.html#" title="menu item">Tradition of School Events</a></li>
                    <li><a href="events.html#" title="menu item">Report Asocial Behaviour</a></li>
                    <li><a href="events.html#" title="menu item">Trends and Tips</a></li>
                    <li><a href="events.html#" title="menu item">Events Poll</a></li>
                </ul>

            </li>

            <li class="widget-container widget_recent_news"><!-- widgets list -->

                <h1 class="title-widget">School News</h1>

                <ul class="list-unstyled">

                    <li class="recent-news-wrap news-no-summary">

                        <div class="recent-news-content clearfix">
                            <figure class="recent-news-thumb">
                                <a href="events.html#" title="Megan Boyle flourishes..."><img src="img/recent-news-thumb-1.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 1" /></a>
                            </figure>
                            <div class="recent-news-text">
                                <div class="recent-news-meta">
                                    <div class="recent-news-date">Jun 12, 2014</div>
                                </div>
                                <h1 class="title-median"><a href="events.html#" title="Megan Boyle flourishes...">Megan Boyle flourishes at Boston University</a></h1>
                            </div>
                        </div>

                    </li>

                    <li class="recent-news-wrap news-no-summary">

                        <div class="recent-news-content clearfix">
                            <figure class="recent-news-thumb">
                                <a href="events.html#" title="Buntington Alum..."><img src="img/recent-news-thumb-2.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 2" /></a>
                            </figure>
                            <div class="recent-news-text">
                                <div class="recent-news-meta">
                                    <div class="recent-news-date">Jun 10, 2014</div>
                                </div>
                                <h1 class="title-median"><a href="events.html#" title="Buntington Alum...">Buntington Alum Marc Bloom Pens New Book</a></h1>
                            </div>
                        </div>

                    </li>

                    <li class="recent-news-wrap news-no-summary">

                        <div class="recent-news-content clearfix">
                            <figure class="recent-news-thumb">
                                <a href="events.html#" title="Cody Rotschild Enjoys..."><img src="img/recent-news-thumb-3.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 3" /></a>
                            </figure>
                            <div class="recent-news-text">
                                <div class="recent-news-meta">
                                    <div class="recent-news-date">Jun 05, 2014</div>
                                </div>
                                <h1 class="title-median"><a href="events.html#" title="Cody Rotschild Enjoys...">Cody Rotschild Enjoys Life in Montreal</a></h1>
                            </div>
                        </div>

                    </li>

                </ul>

            </li><!-- widgets list end -->

            <li class="widget-container widget_newsletter"><!-- widget -->

                <h1 class="title-titan">School Newsletter</h1>

                <form role="search" method="get" class="newsletter-form" action="events.html#">
                    <div class="input-group">
                        <input type="text" placeholder="Your e-mail address" autocomplete="off" class="form-control newsletter-form-input" name="email" />
                        <span class="input-group-btn"><button type="submit" class="btn btn-default">GO!</button></span>
                    </div>
                    <span class="help-block">* Enter your e-mail address to subscribe.</span>
                </form>

            </li>

            <li class="widget-container widget_text"><!-- widget -->

                <a href="events.html#" class="custom-button cb-red" title="How to apply?">
                    <i class="custom-button-icon fa fa-empire"></i>
                                    <span class="custom-button-wrap">
                                    	<span class="custom-button-title">Donate Now</span>
                                        <span class="custom-button-tagline">Become a corporate sponsor of our schools!</span>
                                    </span>
                    <em></em>
                </a>

            </li>

        </ul><!-- widgets end -->

    </div><!-- inner custom column end -->

</div><!-- sidebar wrapper end -->

</div><!-- row end -->

</div><!-- container end -->

</div><!-- content wrapper end -->

<jsp:include page="footer.jsp"/>

<!-- jQuery -->
<script src="jQuery/jquery-2.1.1.min.js"></script>
<script src="jQuery/jquery-migrate-1.2.1.min.js"></script>

<!-- Bootstrap -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<!-- Drop-down -->
<script src="js/dropdown-menu/dropdown-menu.js"></script>

<!-- Fancybox -->
<script src="js/fancybox/jquery.fancybox.pack.js"></script>
<script src="js/fancybox/jquery.fancybox-media.js"></script><!-- Fancybox media -->

<!-- Responsive videos -->
<script src="js/jquery.fitvids.js"></script>

<!-- Audio player -->
<script src="js/audioplayer/audioplayer.min.js"></script>

<!-- Pie charts -->
<script src="js/jquery.easy-pie-chart.js"></script>

<!-- Google Maps -->
<script src="maps/api/js.JS"></script>

<!-- Theme -->
<script src="js/theme.js"></script>

</body>
</html>