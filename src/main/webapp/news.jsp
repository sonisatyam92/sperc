<%@ page import="org.sperc.bean.CourseBean" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BUNTINGTON Public Schools</title>

    <!-- Styles -->
    <link href="css.css" rel="stylesheet" type="text/css"><!-- Google web fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- font-awesome -->
    <link href="js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css"><!-- dropdown-menu -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- Bootstrap -->
    <link href="js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"><!-- Fancybox -->
    <link href="js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css"><!-- Audioplayer -->
    <link href="style.css" rel="stylesheet" type="text/css"><!-- theme styles -->

</head>

<body role="document">

<!-- device test, don't remove. javascript needed! -->
<span class="visible-xs"></span><span class="visible-sm"></span><span class="visible-md"></span><span class="visible-lg"></span>
<!-- device test end -->

<jsp:include page="header.jsp"/>

<div id="k-body"><!-- content wrapper -->

<div class="container"><!-- container -->

<div class="row"><!-- row -->

    <div id="k-top-search" class="col-lg-12 clearfix"><!-- top search -->

        <form action="news.html#" id="top-searchform" method="get" role="search">
            <div class="input-group">
                <input type="text" name="s" id="sitesearch" class="form-control" autocomplete="off" placeholder="Type in keyword(s) then hit Enter on keyboard" />
            </div>
        </form>

        <div id="bt-toggle-search" class="search-icon text-center"><i class="s-open fa fa-search"></i><i class="s-close fa fa-times"></i></div><!-- toggle search button -->

    </div><!-- top search end -->

    <div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->

        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li class="active">News</li>
        </ol>

    </div><!-- breadcrumbs end -->

</div><!-- row end -->

<div class="row no-gutter"><!-- row -->

<div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

    <div class="col-padded"><!-- inner custom column -->

        <div class="row gutter"><!-- row -->

            <div class="col-lg-12 col-md-12">

                <h1 class="page-title">News</h1><!-- category title -->

                <div class="category-description"><!-- category description -->
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pretium vulputate scelerisque. Nulla in suscipit risus. Nullam pulvinar augue in risus luctus, sed dapibus ipsum mattis.
                    </p>
                </div>

            </div>

        </div><!-- row end -->

        <div class="row gutter k-equal-height"><!-- row -->

            <div class="news-mini-wrap col-lg-6 col-md-6"><!-- news mini-wrap -->

                <figure class="news-featured-image">
                    <img src="img/news-1.jpg" alt="Featured image 1" class="img-responsive" />
                </figure>

                <div class="page-title-meta">
                    <h1 class="page-title"><a href="news.html#" title="Cody Rotschild enjoys...">Cody Rotschild enjoys life in Montreal University</a></h1>
                    <div class="news-meta">
                        <span class="news-meta-date">Jun 12, 2014</span>
                        <span class="news-meta-category"><a href="news.html" title="News">News</a></span>
                        <span class="news-meta-comments"><a href="news.html#" title="3 comments">3 comments</a></span>
                    </div>
                </div>

                <div class="news-summary">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pretium vulputate scelerisque. Nulla in suscipit risus... <a href="news.html" title="Read more" class="moretag">More</a>
                    </p>
                </div>

            </div><!-- news mini-wrap end -->

            <div class="news-mini-wrap col-lg-6 col-md-6"><!-- news mini-wrap -->

                <figure class="news-featured-image">
                    <img src="img/news-2.jpg" alt="Featured image 2" class="img-responsive" />
                </figure>

                <div class="page-title-meta">
                    <h1 class="page-title"><a href="news.html#" title="Megan Boyle flourishes...">Megan Boyle flourishes at Boston University</a></h1>
                    <div class="news-meta">
                        <span class="news-meta-date">Jun 10, 2014</span>
                        <span class="news-meta-category"><a href="news.html" title="News">News</a></span>
                        <span class="news-meta-comments"><a href="news.html#" title="12 comments">12 comments</a></span>
                    </div>
                </div>

                <div class="news-summary">
                    <p>
                        Cras lectus diam, semper eget nunc et, ullamcorper faucibus ipsum. Integer id odio venenatis, vestibulum velit quis tempus euismod elit, eu venenatis dolor... <a href="news.html" title="Read more" class="moretag">More</a>
                    </p>
                </div>

            </div><!-- news mini-wrap end -->

            <div class="news-mini-wrap col-lg-6 col-md-6"><!-- news mini-wrap -->

                <figure class="news-featured-image">
                    <img src="img/news-3.jpg" alt="Featured image 3" class="img-responsive" />
                </figure>

                <div class="page-title-meta">
                    <h1 class="page-title"><a href="news.html#" title="Buntington alum Marc...">Buntington alum Marc Bloom pens new book</a></h1>
                    <div class="news-meta">
                        <span class="news-meta-date">Jun 09, 2014</span>
                        <span class="news-meta-category"><a href="news.html" title="News">News</a></span>
                        <span class="news-meta-comments"><a href="news.html#" title="3 comments">3 comments</a></span>
                    </div>
                </div>

                <div class="news-summary">
                    <p>
                        Nam tempus euismod elit, eu venenatis dolor cursus sed. Nam sit amet ipsum ornare, interdum ipsum congue, fermentum turpis... <a href="news.html" title="Read more" class="moretag">More</a>
                    </p>
                </div>

            </div><!-- news mini-wrap end -->

            <div class="news-mini-wrap col-lg-6 col-md-6"><!-- news mini-wrap -->

                <figure class="news-featured-image">
                    <img src="img/news-4.jpg" alt="Featured image 4" class="img-responsive" />
                </figure>

                <div class="page-title-meta">
                    <h1 class="page-title"><a href="news.html#" title="Students have enough time...">Students have enough time to enjoy the summer holidays</a></h1>
                    <div class="news-meta">
                        <span class="news-meta-date">Jun 08, 2014</span>
                        <span class="news-meta-category"><a href="news.html" title="News">News</a></span>
                        <span class="news-meta-comments"><a href="news.html#" title="27 comments">27 comments</a></span>
                    </div>
                </div>

                <div class="news-summary">
                    <p>
                        Ut vitae venenatis dolor. Phasellus volutpat sapien sit amet tellus gravida, nec dignissim elit congue... <a href="news.html" title="Read more" class="moretag">More</a>
                    </p>
                </div>

            </div><!-- news mini-wrap end -->

            <div class="news-mini-wrap col-lg-6 col-md-6"><!-- news mini-wrap -->

                <figure class="news-featured-image">
                    <img src="img/news-5.jpg" alt="Featured image 5" class="img-responsive" />
                </figure>

                <div class="page-title-meta">
                    <h1 class="page-title"><a href="news.html#" title="Hard work is what...">Hard work is what brings great results</a></h1>
                    <div class="news-meta">
                        <span class="news-meta-date">Jun 07, 2014</span>
                        <span class="news-meta-category"><a href="news.html" title="News">News</a></span>
                        <span class="news-meta-comments"><a href="news.html#" title="13 comments">13 comments</a></span>
                    </div>
                </div>

                <div class="news-summary">
                    <p>
                        Curabitur venenatis, leo sit amet gravida elementum, mauris nunc bibendum metus, ut tempus felis turpis ut dolor. Aenean eu sem non... <a href="news.html" title="Read more" class="moretag">More</a>
                    </p>
                </div>

            </div><!-- news mini-wrap end -->

            <div class="news-mini-wrap col-lg-6 col-md-6"><!-- news mini-wrap -->

                <figure class="news-featured-image">
                    <img src="img/news-6.jpg" alt="Featured image 6" class="img-responsive" />
                </figure>

                <div class="page-title-meta">
                    <h1 class="page-title"><a href="news.html#" title="Knowledge exchange...">Knowledge exchange, student meetings and games</a></h1>
                    <div class="news-meta">
                        <span class="news-meta-date">Jun 06, 2014</span>
                        <span class="news-meta-category"><a href="news.html" title="News">News</a></span>
                        <span class="news-meta-comments"><a href="news.html#" title="13 comments">13 comments</a></span>
                    </div>
                </div>

                <div class="news-summary">
                    <p>
                        Pellentesque congue mattis lacus, in blandit elit placerat a. Etiam fermentum imperdiet sollicitudin lorem ipsum nullam est... <a href="news.html" title="Read more" class="moretag">More</a>
                    </p>
                </div>

            </div><!-- news mini-wrap end -->

        </div><!-- row end -->


        <div class="row gutter"><!-- row -->

            <div class="col-lg-12">

                <ul class="pagination pull-right"><!-- pagination -->
                    <li class="disabled"><a href="news.html#">Prev</a></li>
                    <li class="active"><a href="news.html#">1</a></li>
                    <li><a href="news.html#">2</a></li>
                    <li><a href="news.html#">3</a></li>
                    <li><a href="news.html#">4</a></li>
                    <li><a href="news.html#">5</a></li>
                    <li><a href="news.html#">Next</a></li>
                </ul><!-- pagination end -->

            </div>

        </div><!-- row end -->

    </div><!-- inner custom column end -->

</div><!-- doc body wrapper end -->

<div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->

    <div class="col-padded col-shaded"><!-- inner custom column -->

        <ul class="list-unstyled clear-margins"><!-- widgets -->

            <li class="widget-container widget_nav_menu"><!-- widget -->

                <h1 class="title-widget">Useful links</h1>

                <ul>
                    <li><a href="news.html#" title="menu item">News Archive</a></li>
                    <li><a href="news.html#" title="menu item">Tradition of School Events</a></li>
                    <li><a href="news.html#" title="menu item">Report Asocial Behaviour</a></li>
                    <li><a href="news.html#" title="menu item">Trends and Tips</a></li>
                    <li><a href="news.html#" title="menu item">Events Poll</a></li>
                </ul>

            </li>

            <li class="widget-container widget_up_events"><!-- widget -->

                <h1 class="title-widget">Upcoming Events</h1>

                <ul class="list-unstyled">

                    <li class="up-event-wrap">

                        <h1 class="title-median"><a href="news.html#" title="Annual alumni game">Annual alumni game</a></h1>

                        <div class="up-event-meta clearfix">
                            <div class="up-event-date">Jul 25, 2015</div><div class="up-event-time">9:00 - 11:00</div>
                        </div>

                        <p>
                            Fusce condimentum pulvinar mattis. Nunc condimentum sapien sit amet odio vulputate, nec suscipit orci pharetra... <a href="news.html#" class="moretag" title="read more">MORE</a>
                        </p>

                    </li>

                    <li class="up-event-wrap">

                        <h1 class="title-median"><a href="news.html#" title="School talents gathering">School talents gathering</a></h1>

                        <div class="up-event-meta clearfix">
                            <div class="up-event-date">Aug 25, 2015</div><div class="up-event-time">8:30 - 10:30</div>
                        </div>

                        <p>
                            Pellentesque lobortis, arcu eget condimentum auctor, magna neque faucibus dui, ut varius diam neque sed diam... <a href="news.html#" class="moretag" title="read more">MORE</a>
                        </p>

                    </li>

                    <li class="up-event-wrap">

                        <h1 class="title-median"><a href="news.html#" title="School talents gathering">Campus "Open Doors"</a></h1>

                        <div class="up-event-meta clearfix">
                            <div class="up-event-date">Sep 04, 2015</div><div class="up-event-date">Sep 11, 2015</div>
                        </div>

                        <p>
                            Donec fringilla lacinia laoreet. Vestibulum ultrices blandit tempor. Aenean magna elit, varius eget quam a, posuere... <a href="news.html#" class="moretag" title="read more">MORE</a>
                        </p>

                    </li>

                </ul>

            </li>

            <li class="widget-container widget_newsletter"><!-- widget -->

                <h1 class="title-titan">School Newsletter</h1>

                <form role="search" method="get" class="newsletter-form" action="news.html#">
                    <div class="input-group">
                        <input type="text" placeholder="Your e-mail address" autocomplete="off" class="form-control newsletter-form-input" name="email" />
                        <span class="input-group-btn"><button type="submit" class="btn btn-default">GO!</button></span>
                    </div>
                    <span class="help-block">* Enter your e-mail address to subscribe.</span>
                </form>

            </li>

            <li class="widget-container widget_text"><!-- widget -->

                <a href="news.html#" class="custom-button cb-red" title="How to apply?">
                    <i class="custom-button-icon fa fa-empire"></i>
                                    <span class="custom-button-wrap">
                                    	<span class="custom-button-title">Donate Now</span>
                                        <span class="custom-button-tagline">Become a corporate sponsor of our schools!</span>
                                    </span>
                    <em></em>
                </a>

            </li>

        </ul><!-- widgets end -->

    </div><!-- inner custom column end -->

</div><!-- sidebar wrapper end -->

</div><!-- row end -->

</div><!-- container end -->

</div><!-- content wrapper end -->

<jsp:include page="footer.jsp"/>

<!-- jQuery -->
<script src="jQuery/jquery-2.1.1.min.js"></script>
<script src="jQuery/jquery-migrate-1.2.1.min.js"></script>

<!-- Bootstrap -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<!-- Drop-down -->
<script src="js/dropdown-menu/dropdown-menu.js"></script>

<!-- Fancybox -->
<script src="js/fancybox/jquery.fancybox.pack.js"></script>
<script src="js/fancybox/jquery.fancybox-media.js"></script><!-- Fancybox media -->

<!-- Responsive videos -->
<script src="js/jquery.fitvids.js"></script>

<!-- Audio player -->
<script src="js/audioplayer/audioplayer.min.js"></script>

<!-- Pie charts -->
<script src="js/jquery.easy-pie-chart.js"></script>

<!-- Google Maps -->
<script src="maps/api/js.JS"></script>

<!-- Theme -->
<script src="js/theme.js"></script>

</body>
</html>