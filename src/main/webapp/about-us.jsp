<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SPERC | About Us</title>

    <!-- Styles -->
    <link href="css.css" rel="stylesheet" type="text/css"><!-- Google web fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- font-awesome -->
    <link href="js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css"><!-- dropdown-menu -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- Bootstrap -->
    <link href="js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"><!-- Fancybox -->
    <link href="js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css"><!-- Audioplayer -->
    <link href="style.css" rel="stylesheet" type="text/css"><!-- theme styles -->

</head>

<body role="document" class="page">

<!-- device test, don't remove. javascript needed! -->
<span class="visible-xs"></span><span class="visible-sm"></span><span class="visible-md"></span><span class="visible-lg"></span>
<!-- device test end -->

<jsp:include page="header.jsp"/>

<div id="k-body"><!-- content wrapper -->

    <div class="container"><!-- container -->

        <div class="row"><!-- row -->

            <div id="k-top-search" class="col-lg-12 clearfix"><!-- top search -->

                <form action="about-us.html#" id="top-searchform" method="get" role="search">
                    <div class="input-group">
                        <input type="text" name="s" id="sitesearch" class="form-control" autocomplete="off" placeholder="Type in keyword(s) then hit Enter on keyboard" />
                    </div>
                </form>

                <div id="bt-toggle-search" class="search-icon text-center"><i class="s-open fa fa-search"></i><i class="s-close fa fa-times"></i></div><!-- toggle search button -->

            </div><!-- top search end -->

            <div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->

                <ol class="breadcrumb">
                    <li><a href="home">Home</a></li>
                    <li class="active">About Us</li>
                </ol>

            </div><!-- breadcrumbs end -->

        </div><!-- row end -->

        <div class="row no-gutter"><!-- row -->

            <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

                <div class="col-padded"><!-- inner custom column -->

                    <div class="row gutter"><!-- row -->

                        <div class="col-lg-12 col-md-12">

                            <figure class="news-featured-image">
                                <img src="img/news-1.jpg" alt="Featured image 1" class="img-responsive" />
                            </figure>

                            <h1 class="page-title">About SPERC</h1>

                            <div class="news-body">

                                <p class="call-out">
                                    Shree Patidar Education & Research Center, Rajkot.
                                    An autonomous institute constituted Registered under provision I.T. Act 1882Govt. Of India, NCT Of Delhi, Cr Act 1957 Ministry Of Human Resource Department ( M.H.R.D. ) Govt.Of India, B.P.T. Act 1950 Govt.Of Gujarat.
                                    The Shree Patidar Education & Research Center conduct examinations, issues guidelines, prepare syllabus, declare results, introduce yogic & naturopathy, paramedical courses, affiliation institutes.

                                </p>

                                <p>
                                    Duis ornare magna sit amet dui eleifend imperdiet. Aliquam at porta elit. Proin lorem lacus, tempus id diam sit amet, porttitor tempor lectus. Praesent id felis sagittis, suscipit ligula sed, condimentum nisi. In non commodo risus. Praesent fringilla ligula in orci consectetur pulvinar. Nunc facilisis metus pellentesque, vestibulum libero eget, varius elit. Aliquam sed gravida dui, a imperdiet eros. Cras dignissim libero id feugiat pharetra. Nullam ut bibendum est, sed tincidunt massa.
                                </p>

                            </div>

                        </div>

                    </div><!-- row end -->

                    <div class="row gutter k-equal-height"><!-- row -->

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <h6>First Column</h6>
                            <p>
                                Curabitur fringilla, massa sit amet ultricies iaculis, nisi lorem faucibus justo, eu interdum odio nunc eu lorem. Aliquam auctor id augue posuere. Duis ornare magna sit amet dui eleifend imperdiet. Aliquam at porta elit. Proin lorem lacus, tempus id diam sit amet, porttitor tempor lectus.
                            </p>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <h6>Second Column</h6>
                            <p>
                                Maecenas mollis nisl ut diam condimentum, eu sagittis ante elementum. Nam tempus euismod elit, eu venenatis dolor cursus sed. Praesent id felis sagittis, suscipit ligula sed, condimentum nisi. In non commodo risus. Praesent fringilla ligula in orci consectetur pulvinar.
                            </p>
                        </div>

                    </div><!-- row end -->

                </div><!-- inner custom column end -->

            </div><!-- doc body wrapper end -->

           <%-- <div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->

                <div class="col-padded col-shaded"><!-- inner custom column -->

                    <ul class="list-unstyled clear-margins"><!-- widgets -->

                        <li class="widget-container widget_nav_menu"><!-- widget -->

                            <h1 class="title-widget">Select</h1>

                            <ul>
                                <li><a href="about-us.html#" title="menu item">News Archive</a></li>
                                <li><a href="about-us.html#" title="menu item">Tradition of School Events</a></li>
                                <li><a href="about-us.html#" title="menu item">Report Asocial Behaviour</a></li>
                                <li><a href="about-us.html#" title="menu item">Trends and Tips</a></li>
                                <li><a href="about-us.html#" title="menu item">Events Poll</a></li>
                            </ul>

                        </li>

                        <li class="widget-container widget_recent_news"><!-- widgets list -->

                            <h1 class="title-widget">School News</h1>

                            <ul class="list-unstyled">

                                <li class="recent-news-wrap news-no-summary">

                                    <div class="recent-news-content clearfix">
                                        <figure class="recent-news-thumb">
                                            <a href="about-us.html#" title="Megan Boyle flourishes..."><img src="img/recent-news-thumb-1.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 1" /></a>
                                        </figure>
                                        <div class="recent-news-text">
                                            <div class="recent-news-meta">
                                                <div class="recent-news-date">Jun 12, 2014</div>
                                            </div>
                                            <h1 class="title-median"><a href="about-us.html#" title="Megan Boyle flourishes...">Megan Boyle flourishes at Boston University</a></h1>
                                        </div>
                                    </div>

                                </li>

                                <li class="recent-news-wrap news-no-summary">

                                    <div class="recent-news-content clearfix">
                                        <figure class="recent-news-thumb">
                                            <a href="about-us.html#" title="Buntington Alum..."><img src="img/recent-news-thumb-2.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 2" /></a>
                                        </figure>
                                        <div class="recent-news-text">
                                            <div class="recent-news-meta">
                                                <div class="recent-news-date">Jun 10, 2014</div>
                                            </div>
                                            <h1 class="title-median"><a href="about-us.html#" title="Buntington Alum...">Buntington Alum Marc Bloom Pens New Book</a></h1>
                                        </div>
                                    </div>

                                </li>

                                <li class="recent-news-wrap news-no-summary">

                                    <div class="recent-news-content clearfix">
                                        <figure class="recent-news-thumb">
                                            <a href="about-us.html#" title="Cody Rotschild Enjoys..."><img src="img/recent-news-thumb-3.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 3" /></a>
                                        </figure>
                                        <div class="recent-news-text">
                                            <div class="recent-news-meta">
                                                <div class="recent-news-date">Jun 05, 2014</div>
                                            </div>
                                            <h1 class="title-median"><a href="about-us.html#" title="Cody Rotschild Enjoys...">Cody Rotschild Enjoys Life in Montreal</a></h1>
                                        </div>
                                    </div>

                                </li>

                            </ul>

                        </li><!-- widgets list end -->

                        <li class="widget-container widget_sofa_twitter"><!-- widget -->

                            <ul class="k-twitter-twitts list-unstyled">

                                <li class="twitter-twitt">
                                    <p>
                                        <a href="../../twitter.com/DanielleFilson.htm">@MattDeamon</a> Why it always has to be so complicated? Try to get it via this link <a href="http://usap.co/potter">mama.co/hpot</a> Good luck mate!
                                    </p>
                                </li>

                            </ul>

                            <div class="k-twitter-twitts-footer">
                                <a href="about-us.html#" class="k-twitter-twitts-follow" title="Follow!"><i class="fa fa-twitter"></i>&nbsp; Follow us!</a>
                            </div>

                        </li><!-- widget end -->

                    </ul><!-- widgets end -->

                </div><!-- inner custom column end -->

            </div><!-- sidebar wrapper end -->--%>

        </div><!-- row end -->

    </div><!-- container end -->

</div><!-- content wrapper end -->

<jsp:include page="footer.jsp"/>

<!-- jQuery -->
<script src="jQuery/jquery-2.1.1.min.js"></script>
<script src="jQuery/jquery-migrate-1.2.1.min.js"></script>

<!-- Bootstrap -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<!-- Drop-down -->
<script src="js/dropdown-menu/dropdown-menu.js"></script>

<!-- Fancybox -->
<script src="js/fancybox/jquery.fancybox.pack.js"></script>
<script src="js/fancybox/jquery.fancybox-media.js"></script><!-- Fancybox media -->

<!-- Responsive videos -->
<script src="js/jquery.fitvids.js"></script>

<!-- Audio player -->
<script src="js/audioplayer/audioplayer.min.js"></script>

<!-- Pie charts -->
<script src="js/jquery.easy-pie-chart.js"></script>

<!-- Google Maps -->
<script src="maps/api/js.JS"></script>

<!-- Theme -->
<script src="js/theme.js"></script>

</body>
</html>