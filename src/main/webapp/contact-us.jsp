<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SPERC</title>

    <!-- Styles -->
    <link href="css.css" rel="stylesheet" type="text/css"><!-- Google web fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- font-awesome -->
    <link href="js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css"><!-- dropdown-menu -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- Bootstrap -->
    <link href="js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"><!-- Fancybox -->
    <link href="js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css"><!-- Audioplayer -->
    <link href="style.css" rel="stylesheet" type="text/css"><!-- theme styles -->

</head>

<body role="document" class="page">

<!-- device test, don't remove. javascript needed! -->
<span class="visible-xs"></span><span class="visible-sm"></span><span class="visible-md"></span><span class="visible-lg"></span>
<!-- device test end -->

<jsp:include page="header.jsp"/>

<div id="k-body"><!-- content wrapper -->

<div class="container"><!-- container -->

<div class="row"><!-- row -->

    <div id="k-top-search" class="col-lg-12 clearfix"><!-- top search -->

        <form action="contact-us.html#" id="top-searchform" method="get" role="search">
            <div class="input-group">
                <input type="text" name="s" id="sitesearch" class="form-control" autocomplete="off" placeholder="Type in keyword(s) then hit Enter on keyboard" />
            </div>
        </form>

        <div id="bt-toggle-search" class="search-icon text-center"><i class="s-open fa fa-search"></i><i class="s-close fa fa-times"></i></div><!-- toggle search button -->

    </div><!-- top search end -->

    <div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->

        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li class="active">Contact Us</li>
        </ol>

    </div><!-- breadcrumbs end -->

</div><!-- row end -->

<div class="row no-gutter"><!-- row -->

<div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

    <div class="col-padded"><!-- inner custom column -->

        <div class="row gutter"><!-- row -->

            <div class="col-lg-12 col-md-12">

                <div id="k-contact-map" class="clearfix"><!-- map -->
                    <div
                            id="g-map-1"
                            class="map"
                            data-gmaptitle="Contact"
                            data-gmapzoom="15"
                            data-gmaplon="70.967031"
                            data-gmaplat="22.726084"
                            data-gmapmarker=""
                            data-cname="SPERC"
                            data-caddress="NH8A- Matel- Dhuva Rd"
                            data-ccity="Wakaner"
                            data-cstate="Gujarat"
                            data-czip="363621"
                            data-ccountry="India">
                    </div>
                </div>

                <h1 class="page-title">Contact Us</h1>

                <div class="news-body">

                    <p>
                        Please Feel free reach us at below Contact Details.

                    </p>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">




                            <h6 class="remove-margin-bottom">SPERC</h6>
                            <h7 class="remove-margin-bottom">Shree Patidar Education & Research Center</h7>

                            <p class="small">
                                N.H.-8A, Matel Road, Dhuva
                            </p>
                            <p class="small">
                                TA - Wankaner, DI - Rajkot., Gujarat
                            </p>
                            <p class="small">
                                India. 363621<br/>
                                TEL: +91- 90 99 700108<br/>
                                TEL: +91- 92 75 000108<br/>
                                TEL: +91- 90 33 300108
                            </p>


                        </div>


                    </div>

                    <hr />

                    <h6>Drop us note!</h6>

                    <form id="contactform" method="post" action="contact-us.html#">
                        <div class="row"><!-- starts row -->
                            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                <label for="contactName"><span class="required">*</span> Name</label>
                                <input type="text" aria-required="true" size="30" value="" name="contactName" id="contactName" class="form-control requiredField" />
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                <label for="email"><span class="required">*</span> E-mail</label>
                                <input type="text" aria-required="true" size="30" value="" name="email" id="email" class="form-control requiredField" />
                            </div>
                        </div><!-- ends row -->

                        <div class="row"><!-- starts row -->
                            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                <label for="contactPhone">Phone / Cell</label>
                                <input type="text" aria-required="true" size="30" value="" name="contactPhone" id="contactPhone" class="form-control" />
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                <label for="contactSchool">Select School</label>
                                <select name="contactSchool" id="contactSchool" class="form-control">
                                    <option value="High School">High School</option>
                                    <option value="Middle School">Middle School</option>
                                    <option value="Intermediate School">Intermediate School</option>
                                    <option value="Primary School">Primary School</option>
                                </select>
                            </div>
                        </div><!-- ends row -->

                        <div class="row"><!-- starts row -->
                            <div class="form-group col-lg-12">
                                <label for="contactSubject">Message Subject</label>
                                <input type="text" aria-required="true" size="30" value="" name="contactSubject" id="contactSubject" class="form-control" />
                            </div>
                        </div><!-- ends row -->

                        <div class="row"><!-- starts row -->
                            <div class="form-group clearfix col-lg-12">
                                <label for="comments"><span class="required">*</span> Message</label>
                                <textarea aria-required="true" rows="5" cols="45" name="comments" id="comments" class="form-control requiredField mezage"></textarea>
                            </div>

                            <div class="form-group clearfix col-lg-12 text-right remove-margin-bottom">
                                <input type="hidden" name="submitted" id="submitted" value="true" />
                                <input type="submit" value="Send Message" id="submit" name="submit" class="btn btn-default" />
                            </div>
                        </div><!-- ends row -->
                    </form>

                </div>

            </div>

        </div><!-- row end -->

    </div><!-- inner custom column end -->

</div><!-- doc body wrapper end -->

<div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->

    <div class="col-padded col-shaded"><!-- inner custom column -->

        <ul class="list-unstyled clear-margins"><!-- widgets -->

            <li class="widget-container widget_nav_menu"><!-- widget -->

                <h1 class="title-widget">Select</h1>

                <ul>
                    <li><a href="contact-us.html#" title="menu item">News Archive</a></li>
                    <li><a href="contact-us.html#" title="menu item">Tradition of School Events</a></li>
                    <li><a href="contact-us.html#" title="menu item">Report Asocial Behaviour</a></li>
                    <li><a href="contact-us.html#" title="menu item">Trends and Tips</a></li>
                    <li><a href="contact-us.html#" title="menu item">Events Poll</a></li>
                </ul>

            </li>

            <li class="widget-container widget_recent_news"><!-- widgets list -->

                <h1 class="title-widget">School News</h1>

                <ul class="list-unstyled">

                    <li class="recent-news-wrap news-no-summary">

                        <div class="recent-news-content clearfix">
                            <figure class="recent-news-thumb">
                                <a href="contact-us.html#" title="Megan Boyle flourishes..."><img src="img/recent-news-thumb-1.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 1" /></a>
                            </figure>
                            <div class="recent-news-text">
                                <div class="recent-news-meta">
                                    <div class="recent-news-date">Jun 12, 2014</div>
                                </div>
                                <h1 class="title-median"><a href="contact-us.html#" title="Megan Boyle flourishes...">Megan Boyle flourishes at Boston University</a></h1>
                            </div>
                        </div>

                    </li>

                    <li class="recent-news-wrap news-no-summary">

                        <div class="recent-news-content clearfix">
                            <figure class="recent-news-thumb">
                                <a href="contact-us.html#" title="Buntington Alum..."><img src="img/recent-news-thumb-2.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 2" /></a>
                            </figure>
                            <div class="recent-news-text">
                                <div class="recent-news-meta">
                                    <div class="recent-news-date">Jun 10, 2014</div>
                                </div>
                                <h1 class="title-median"><a href="contact-us.html#" title="Buntington Alum...">Buntington Alum Marc Bloom Pens New Book</a></h1>
                            </div>
                        </div>

                    </li>

                    <li class="recent-news-wrap news-no-summary">

                        <div class="recent-news-content clearfix">
                            <figure class="recent-news-thumb">
                                <a href="contact-us.html#" title="Cody Rotschild Enjoys..."><img src="img/recent-news-thumb-3.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 3" /></a>
                            </figure>
                            <div class="recent-news-text">
                                <div class="recent-news-meta">
                                    <div class="recent-news-date">Jun 05, 2014</div>
                                </div>
                                <h1 class="title-median"><a href="contact-us.html#" title="Cody Rotschild Enjoys...">Cody Rotschild Enjoys Life in Montreal</a></h1>
                            </div>
                        </div>

                    </li>

                </ul>

            </li><!-- widgets list end -->

            <li class="widget-container widget_sofa_twitter"><!-- widget -->

                <ul class="k-twitter-twitts list-unstyled">

                    <li class="twitter-twitt">
                        <p>
                            <a href="../../twitter.com/DanielleFilson.htm">@MattDeamon</a> Why it always has to be so complicated? Try to get it via this link <a href="http://usap.co/potter">mama.co/hpot</a> Good luck mate!
                        </p>
                    </li>

                </ul>

                <div class="k-twitter-twitts-footer">
                    <a href="contact-us.html#" class="k-twitter-twitts-follow" title="Follow!"><i class="fa fa-twitter"></i>&nbsp; Follow us!</a>
                </div>

            </li><!-- widget end -->

        </ul><!-- widgets end -->

    </div><!-- inner custom column end -->

</div><!-- sidebar wrapper end -->

</div><!-- row end -->

</div><!-- container end -->

</div><!-- content wrapper end -->
<jsp:include page="footer.jsp"/>

<!-- jQuery -->
<script src="jQuery/jquery-2.1.1.min.js"></script>
<script src="jQuery/jquery-migrate-1.2.1.min.js"></script>

<!-- Bootstrap -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<!-- Drop-down -->
<script src="js/dropdown-menu/dropdown-menu.js"></script>

<!-- Fancybox -->
<script src="js/fancybox/jquery.fancybox.pack.js"></script>
<script src="js/fancybox/jquery.fancybox-media.js"></script><!-- Fancybox media -->

<!-- Responsive videos -->
<script src="js/jquery.fitvids.js"></script>

<!-- Audio player -->
<script src="js/audioplayer/audioplayer.min.js"></script>

<!-- Pie charts -->
<script src="js/jquery.easy-pie-chart.js"></script>

<!-- Google Maps -->
<script src="maps/api/js.JS"></script>

<!-- Theme -->
<script src="js/theme.js"></script>

</body>
</html>