<%@ page import="org.sperc.bean.CourseBean" %>
<%@ page import="java.util.List" %>
<div id="k-head" class="container"><!-- container + head wrapper -->

    <div class="row"><!-- row -->

        <nav class="k-functional-navig"><!-- functional navig -->

            <ul class="list-inline pull-right">
                <li><a href="login">Login</a></li>
                <li><a href="career">Careers</a></li>
            </ul>

        </nav><!-- functional navig end -->

        <div class="col-lg-12">
            <div style="height: 60px!important;">
                <div id="k-site-logo" class="pull-left"><!-- site logo -->

                    <h1 class="k-logo">
                        <a href="home" title="Home Page">
                            <img src="img/site-logo.png" alt="Site Logo" class="img-responsive" />
                        </a>
                    </h1>

                    <a id="mobile-nav-switch" href="home#drop-down-left"><span class="alter-menu-icon"></span></a><!-- alternative menu button -->

                </div><!-- site logo end -->
                <div style="position:absolute; margin-left: 230px; font-size: 31px;margin-top:10px;">
                    <p><b>SHREE PATIDAR EDUCATION & RESEARCH CENTER</b>
                    </p>

                </div>
                <div style="position:absolute; margin-left: 230px; margin-top: 36px; font-size: 18px;">
                    <p><b>MANAGED BY : SHREE PATIDAR EDUCATION TRUST</b></p>

                </div>
                <div style="position:absolute; margin-left: 230px;margin-top: 55px; font-size: 15px;">
                    <p>An Autonomous Institute constituted registered under provision of I.T. ACT 1882 Govt.Of India,
                        NCT Of New Delhi, CR ACT 1957 Ministry Of Human Resource Department (MHRD) Govt. Of India,
                        BPT ACT 1950 Govt. Of Gujarat.</p>
                </div>


            </div>
            <div>

                <nav id="k-menu" class="k-main-navig"><!-- main navig -->

                    <ul id="drop-down-left" class="k-dropdown-menu">

                        <li>
                            <a href="home" title="Home">Home</a>
                        </li>
                        <li>
                            <a href="news" title="News">News</a>
                        </li>
                        <li>
                            <a href="events" title="Upcoming Events">Events</a>
                        </li>
                        <li>
                            <ul class="sub-menu">
                                <%

                                    List<CourseBean> courses = (List<CourseBean>) request.getAttribute("courses");
                                    if(courses != null)
                                        for(final CourseBean c : courses){%>
                                <li><a href="courseDetail?code=<%=c.getId()%>"><%=c.getName()%></a></li><%}%>
                            </ul>
                            <a href="courses" title="Available Courses">Courses</a>
                        </li>
                        <li>
                            <a href="results" class="Pages Collection" title="Results">Results</a>
                            <%--<ul class="sub-menu">
                                <li><a href="news-single.html">News Single Page</a></li>
                                <li><a href="events-single.html">Events Single Page</a></li>
                                <li><a href="courses-single.html">Course Single Page</a></li>
                                <li><a href="gallery-single.html">Gallery Single Page</a></li>
                                <li><a href="news-stacked.html">News Stacked Page</a></li>
                                <li><a href="search-results.html">Search Results Page</a></li>
                                <li>
                                    <a href="index.html#">Menu Test</a>
                                    <ul class="sub-menu">
                                        <li><a href="index.html#">Second Level 01</a></li>
                                        <li>
                                            <a href="index.html#">Second Level 02</a>
                                            <ul class="sub-menu">
                                                <li><a href="index.html#">Third Level 01</a></li>
                                                <li><a href="index.html#">Third Level 02</a></li>
                                                <li><a href="index.html#">Third Level 03</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="index.html#">Second Level 03</a></li>
                                    </ul>
                                </li>
                            </ul>--%>
                        </li>
                        <li>
                            <a href="aboutus" title="About Us">About Us</a>
                            <ul class="sub-menu">
                                <%-- <li><a href="full-width.html">Full Width Page</a></li>
                                 <li><a href="sidebar-left.html">Sidebar on Left</a></li>
                                 <li><a href="formatting.html">Formatting</a></li>--%>
                                <li><a href="faculties">Faculty</a></li>
                                <%--<li><a href="gallery.html">Gallery</a></li>
                                <li><a href="404.html">404 Error</a></li>--%>
                            </ul>
                        </li>
                        <li>
                            <a href="ContactUs" title="School Contacts">Contact Us</a>
                        </li>
                    </ul>

                </nav><!-- main navig end -->


            </div>

        </div>

    </div><!-- row end -->

</div><!-- container + head wrapper end -->
